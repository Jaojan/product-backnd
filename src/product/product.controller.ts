import {
  Controller,
  Post,
  Get,
  Param,
  Put,
  Delete,
  Body,
  Res,
  HttpStatus,
  NotFoundException,
  Query,
} from '@nestjs/common';
import { CreateProductDTO } from './dto/product.dto';
import { ProductService } from './product.service';

@Controller('product')
export class ProductController {
  constructor(private productService: ProductService) {}

  @Get('/')
  async getProducts(@Res() res) {
    const products = await this.productService.getProducts();
    return res.status(HttpStatus.OK).json({
      products: products,
    });
  }

  @Get('/:productID')
  async getProduct(@Res() res, @Param('productID') productID) {
    const product = await this.productService.getProduct(productID);
    //If not have Product ID
    if (!product) {
      throw new NotFoundException('Product Does not exist');
    }
    return res.status(HttpStatus.OK).json(product);
  }

  @Post('/create')
  async createProduct(@Res() res, @Body() createProductDTO: CreateProductDTO) {
    const product = await this.productService.createProduct(createProductDTO);
    return res.status(HttpStatus.OK).json({
      message: 'Product Create Success',
      product: product,
    });
  }

  @Delete('/delete')
  async deletProduct(@Res() res, @Query('productID') productID) {
    const productDelete = await this.productService.deleteProduct(productID);
    if (!productDelete) {
      throw new NotFoundException('Product Does not exist');
    }
    return res.status(HttpStatus.OK).json({
      message: 'Delete Product Success',
      productDelete,
    });
  }

  @Put('/update')
 async updateProduct(
    @Res() res,
    @Body() createProductDTO: CreateProductDTO,
    @Query('productID') productID,
  ) {
    const productUpdate = await this.productService.updateProduct(productID, createProductDTO);
    if (!productUpdate) {
        throw new NotFoundException('Product Does not exist');
      }
     return res.status(HttpStatus.OK).json({
        message: 'Update Product Success',
        productUpdate,
     }) 
}
}
